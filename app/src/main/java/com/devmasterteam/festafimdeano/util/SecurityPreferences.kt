package com.devmasterteam.festafimdeano.util

import android.content.Context

/**
 * Created by guguh on 28/08/2017.
 */
class SecurityPreferences (val context: Context){
    private val mSharedPreferences = context.getSharedPreferences("festa", Context.MODE_PRIVATE)

    fun getString(key: String): String = mSharedPreferences.getString(key, "")
    fun storeString(key: String, value: String) = mSharedPreferences.edit().putString(key, value).apply()
}