package com.devmasterteam.festafimdeano.ui

import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import com.devmasterteam.festafimdeano.R
import com.devmasterteam.festafimdeano.constants.PartyConstants
import com.devmasterteam.festafimdeano.util.SecurityPreferences

class DetailsActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var checkbox_confirm: CheckBox
    private lateinit var mSharedPreferences: SecurityPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        mSharedPreferences = SecurityPreferences(this)
        checkbox_confirm = findViewById(R.id.check_participate) as CheckBox
        checkbox_confirm.setOnClickListener(this)

        with(supportActionBar) {
            this?.setDisplayShowTitleEnabled(false)
            this?.setDisplayShowHomeEnabled(true)
            this?.setIcon(R.mipmap.ic_launcher)
        }

        loadDataFromPreviousActivity()
    }

    private fun loadDataFromPreviousActivity() {
        val extras = intent.extras
        if(extras != null) {
            val checkboxValue = mSharedPreferences.getString(PartyConstants.CONFIRM_KEY)
            checkbox_confirm.isChecked = (checkboxValue == PartyConstants.CONFIRM_WILL_GO)
        }
    }

    override fun onClick(v: View?) {
        val int = v?.id

        when(int) {
            R.id.check_participate -> {
                if(checkbox_confirm.isChecked)
                    mSharedPreferences.storeString(PartyConstants.CONFIRM_KEY, PartyConstants.CONFIRM_WILL_GO)
                else
                    mSharedPreferences.storeString(PartyConstants.CONFIRM_KEY, PartyConstants.CONFIRM_WONT_GO)
            }
        }
    }
}
