package com.devmasterteam.festafimdeano.ui

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.devmasterteam.festafimdeano.R
import com.devmasterteam.festafimdeano.constants.PartyConstants
import com.devmasterteam.festafimdeano.util.SecurityPreferences
import java.util.*

class MainActivity : AppCompatActivity(), View.OnClickListener{
    private lateinit var buttonConfirm: Button
    private lateinit var textDaysLeft: TextView
    private lateinit var textToday: TextView
    private lateinit var mSharedPreferences: SecurityPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        with(supportActionBar) {
            this?.setDisplayShowTitleEnabled(false)
            this?.setDisplayShowHomeEnabled(true)
            this?.setIcon(R.mipmap.ic_launcher)
        }
        mSharedPreferences = SecurityPreferences(this)
        buttonConfirm = findViewById(R.id.button_confirm) as Button
        textDaysLeft = findViewById(R.id.text_days_left) as TextView
        textToday = findViewById(R.id.text_today) as TextView
        buttonConfirm.setOnClickListener(this)

    }

    override fun onResume() {
        super.onResume()
        loadTextButton()
        textDaysLeft.setText(refreshActualDate())
    }

    private fun loadTextButton(): Unit {
        val checkboxValue = mSharedPreferences.getString(PartyConstants.CONFIRM_KEY)
        when {
            checkboxValue == PartyConstants.CONFIRM_WILL_GO -> buttonConfirm.setText(R.string.sim)
            checkboxValue == PartyConstants.CONFIRM_WONT_GO -> buttonConfirm.setText(R.string.nao)
            checkboxValue == "" -> buttonConfirm.setText(R.string.nao_confirmado)
        }
    }

    private fun refreshActualDate(): String {
        var calendar = Calendar.getInstance()

        var today = calendar.get(Calendar.DAY_OF_YEAR)
        var lastday = calendar.getActualMaximum(Calendar.DAY_OF_YEAR)

        return (lastday - today).toString()
    }

    override fun onClick(v: View?) {
        val id = v?.id
        when (id) {
            R.id.button_confirm -> {
                val gotoDetails: Intent = Intent(this, DetailsActivity::class.java)
                val checkboxValue = mSharedPreferences.getString(PartyConstants.CONFIRM_KEY)

                gotoDetails.putExtra(PartyConstants.CONFIRM_KEY, checkboxValue)

                startActivity(gotoDetails)
            }
        }

    }

}
