package com.devmasterteam.festafimdeano.constants

/**
 * Created by guguh on 31/08/2017.
 */
class PartyConstants {

    companion object {
        val CONFIRM_KEY = "confirm_key"
        val CONFIRM_WILL_GO = "yes"
        val CONFIRM_WONT_GO = "no"
    }
}